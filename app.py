# -*- coding: utf-8 -*-


from flask import Flask, jsonify, render_template, request, send_file,flash
from werkzeug import secure_filename
import time
import os
import numpy as np
import tensorflow as tf
import main_word2vec
app = Flask(__name__)
flag=0
@app.route('/_add_numbers')
def add_numbers():
    global flag    
    print(flag,"this is flag")
    if flag==0: 
        a = request.args.get('a', 0, type=str)
        print(a)
        if len(a.split(' '))<=1:
        	return jsonify(result=0)
        #corpus_raw = a.lower()
        corpus_raw = a.rstrip('\n')
        symbols=['"',',','/','\\','[',']','{','}','*','^','?','%','!','@','-','+','_','(',')','~','`']
        for i in symbols:
        	corpus_raw=corpus_raw.replace(i,'')
        corpus_raw=corpus_raw.replace('.',' . ')
        out_str=main_word2vec.Word2Vec_Malayalam(corpus_raw)
        return jsonify(result=out_str)
    else:
        d=open("Input.txt","r", encoding="utf-8")
        text=d.read()
        d.close()
        os.remove("Input.txt")
        print(text)
        corpus_raw = text
        symbols=['"',',','/','\\','[',']','{','}','*','^','?','%','!','@','-','+','_','(',')','~','`']
        
        for i in symbols:
        	corpus_raw=corpus_raw.replace(i,'')
        corpus_raw=corpus_raw.replace('.',' . ')
        out_str=main_word2vec.Word2Vec_Malayalam(corpus_raw)
        print(out_str)
        #
        flag=0
        return jsonify(result=out_str)
@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        try:
           request.files['file']
           f = request.files['file']
           f.save(secure_filename("Input.txt"))
           global flag
           flag=1
           print(flag,"this is another flag")        
           return '', 204
        except:
            print("no file")
            return '', 204                
      #corpus_raw_lines=data_file.readlines()
      




      #return 'file uploaded successfully'
@app.route('/downloads/<path:filename>', methods=['GET', 'POST'])
def download(filename):
    #uploads = os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'])
    return send_file("downloads/Word2vec_Result.txt", as_attachment=True)

@app.route('/')
def index():
    return render_template('index.html')
app.run(host="0.0.0.0",debug="True")
