**Malayalam Word2vec 1.0**

**Description** <br/>

The transforming from word to vectors is also known as word embedding . The reason for this transformation is so that machine learning algorithm can perform linear algebra operations on numbers instead of words. Word2vec implemented by using python, tensor flow and NumPy.  And here using Continuous Bag Of Words (CBOW) methods. CBOW attempt to guess the output from its neighboring words (context words).   For instance , if the context window C is set to 5 then the input would be words at positions w(t-2), w(t-1), w(t+1), and w(t+2). Basically the two words before and after the center word w(t). Given this information, CBOW then tries to predict the target word


**Screenshots**<br/>




**Main Feature**<br/>
Accept the input in text and file format
Analyse the data  and return corresponding vectorised data for each word
Visible the output in the window and also can download the data in .txt extension

**Dependencies**<br/>
Ubuntu 18.04
Python3.6.5
flask 
werkzeug
Time 
OS
Tensorflow 1.5.0
numpy1.16.0

**Details** <br/>
It Mainly use 
Flask – It is uesd for design UI
werkzeug -- WSGI web application library.
Numpy – Vectorising Data
Tensorflow – Training Data

**Setup to Connect in your local System**<br/>
1. Install all dependencies
2. Run the python code in your terminal
3. Go to the localhost:5000 in your web brower 

**Creator**<br/>
Anju R C

**Project Guidence and Support**<br/>
Dr. Rajeev RR

**Contributors**
Aron : Contributed to building Flask Frame work.



**Developers** <br/>
 This project is  developed by ICFOSS


**Licence**<br/>
GPLv3 only. See License.md


**Development**<br/>
All the information can be found on [ICFOSS-Gitlab](https://gitlab.com/icfoss)
